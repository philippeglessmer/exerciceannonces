<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230529192211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE col_fonction (id INT AUTO_INCREMENT NOT NULL, genre_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_ED277AE44296D31F (genre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE col_genre (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE col_fonction ADD CONSTRAINT FK_ED277AE44296D31F FOREIGN KEY (genre_id) REFERENCES col_genre (id)');
        $this->addSql('ALTER TABLE simulation ADD CONSTRAINT FK_CBDA467BBF396750 FOREIGN KEY (id) REFERENCES simulation (id)');
        $this->addSql('ALTER TABLE simulation_duree ADD CONSTRAINT FK_6C2B5C1EFEC09103 FOREIGN KEY (simulation_id) REFERENCES simulation (id)');
        $this->addSql('ALTER TABLE simulation_financement ADD CONSTRAINT FK_1EB099EBFEC09103 FOREIGN KEY (simulation_id) REFERENCES simulation (id)');
        $this->addSql('ALTER TABLE simulation_montant ADD CONSTRAINT FK_263A2953FEC09103 FOREIGN KEY (simulation_id) REFERENCES simulation (id)');
        $this->addSql('ALTER TABLE simulation_offre ADD CONSTRAINT FK_47FB1A44A7991F51 FOREIGN KEY (collectivite_id) REFERENCES collectivite (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE col_fonction DROP FOREIGN KEY FK_ED277AE44296D31F');
        $this->addSql('DROP TABLE col_fonction');
        $this->addSql('DROP TABLE col_genre');
        $this->addSql('ALTER TABLE simulation DROP FOREIGN KEY FK_CBDA467BBF396750');
        $this->addSql('ALTER TABLE simulation_duree DROP FOREIGN KEY FK_6C2B5C1EFEC09103');
        $this->addSql('ALTER TABLE simulation_financement DROP FOREIGN KEY FK_1EB099EBFEC09103');
        $this->addSql('ALTER TABLE simulation_montant DROP FOREIGN KEY FK_263A2953FEC09103');
        $this->addSql('ALTER TABLE simulation_offre DROP FOREIGN KEY FK_47FB1A44A7991F51');
    }
}
