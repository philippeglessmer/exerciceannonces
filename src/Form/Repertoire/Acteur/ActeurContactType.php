<?php

namespace App\Form\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurContact;
use App\Entity\Repertoire\Acteur\Lib\Contact;
use App\Entity\Repertoire\Acteur\Lib\Identification;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', EntityType::class, [
                'label' => false,
                'placeholder' => 'Choisissez le type de coordonnées',
                'class' => Contact::class,
                'attr' => [
                    'class' => 'mt-3'
                ]
            ])
            ->add('content', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Numéro, lien, info',
                ]
            ])
            ->add('libelle' , TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Titre',
                ]
            ])
//            ->add('acteur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ActeurContact::class,
        ]);
    }
}
