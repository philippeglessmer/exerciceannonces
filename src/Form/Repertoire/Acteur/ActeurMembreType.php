<?php

namespace App\Form\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurMembre;
use App\Entity\Repertoire\Acteur\Lib\Fonctions;
use App\Entity\Repertoire\Acteur\Lib\Identification;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurMembreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('genre', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Choisissez le genre',
                'choices' => ['Monsieur' => 'Monsieur', 'Madame' => 'Madame']
            ])
            ->add('nom', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Nom',
                ],
            ])
            ->add('prenom', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Prénom',
                ],
            ])
            ->add('telephone', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Téléphone fixe',
                ],
            ])
            ->add('portable', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Téléphone Portable',
                ],
            ])
            ->add('mail', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Adresse mail',
                ],
            ])
//            ->add('ordre', NumberType::class, [
//                'label' => false,
//                'attr' => [
//                    'class' => 'mt-3',
//                    'placeholder' => 'Adresse',
//                ],
//            ])
            ->add('fonction', EntityType::class, [
                'label' => false,
                'placeholder' => 'Choisissez la fonction',
                'class' => Fonctions::class,
                'attr' => [

                    'class' => 'mt-3'
                ]
            ])
//            ->add('acteur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ActeurMembre::class,
        ]);
    }
}
