<?php

namespace App\Form\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurIdentification;
use App\Entity\Repertoire\Acteur\Lib\Identification;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurIdentificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identifications', EntityType::class, [
                'label' => false,
                'placeholder' => 'Choisissez le type d\'identification',
                'class' => Identification::class,
                'attr' => [
                    'class' => 'mt-3'
                ]
            ])
            ->add('content', TextType::class,[
                'label' => false,
                'attr' => [
                    'class' => '',
                    'placeholder' => 'Numéro d\'identification'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ActeurIdentification::class,
        ]);
    }
}
