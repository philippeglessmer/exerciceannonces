<?php

namespace App\Form\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\Acteur;
use App\Entity\Repertoire\Acteur\Lib\Categorie;
use App\Entity\Repertoire\Acteur\Lib\Type;
use App\Form\Repertoire\Acteur\Lib\TypeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('raisonSocial', TextType::class,[
                'label' => 'Raison Social',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Raison Social',
                ],

            ])
            ->add('etat', ChoiceType::class, [
                'placeholder' => 'Choisissez l\'état',
                'choices' => ['Actif' => 1, 'Brouillons' => 0],
                'attr' => [
                    'class' => 'mt-3',
                ],
            ])
            ->add('presentation', TextType::class, [
                'label' => 'Présentation de la structure',
                'attr' => [
                    'class' => 'mt-3 textaera'
                ]

            ])
            ->add('juridique', EntityType::class, [
                'placeholder' => 'Choisissez structure juridique',
                'class' => Type::class,
                'attr' => [

                    'class' => 'mt-3'
                ]

            ])
            ->add('categories', EntityType::class, [
                'class' => Categorie::class,
                'multiple'=> true,
                'expanded' => true,
            ])
            ->add('projet', TextType::class, [
                'label' => 'Projet de l\'association',
                'attr' => [
                    'class' => 'd-none textaera'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Acteur::class,
        ]);
    }
}
