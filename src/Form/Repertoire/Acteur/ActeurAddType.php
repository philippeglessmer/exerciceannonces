<?php

namespace App\Form\Repertoire\Acteur;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('acteur', ActeurType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('Adresse', ActeurAdresseType::class, [
                'label' => false,
                'required' => false,
            ])
            ->add('membres', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => ActeurMembreType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => true,
            ])
            ->add('coordonnees', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => ActeurContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('identification', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => ActeurIdentificationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
