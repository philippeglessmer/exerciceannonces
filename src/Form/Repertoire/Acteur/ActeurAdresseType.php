<?php

namespace App\Form\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurAdresse;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurAdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('libelle', TextType::class, [
                'label' => 'Libelle',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Titre de l\'adresse',
                ],
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Adresse',
                ],
            ])
            ->add('codePostal', NumberType::class, [
                'label' => 'Code Postal',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Code Postal',
                ],
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Ville',
                ],
            ])
            ->add('complement', TextType::class, [
                'label' => 'Complement d\'adresse',
                'attr' => [
                    'class' => 'mt-3',
                    'placeholder' => 'Complément d\'information',
                ],
            ])
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('acteur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ActeurAdresse::class,
        ]);
    }
}
