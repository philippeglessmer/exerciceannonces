<?php

namespace App\Form\ExerciceCp;

use App\Entity\ExerciceCp\SimulationOffre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimulationOffreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('numOffre', HiddenType::class, [
                'label' => false,
            ])
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('collectivite')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SimulationOffre::class,
        ]);
    }
}
