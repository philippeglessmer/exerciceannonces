<?php

namespace App\Form\ExerciceCp;

use App\Entity\ExerciceCp\Simulation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimulationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'Type de simulation',
                'placeholder' => '-- Choisissez --',
                'choices' => [
                    'Ligne de trésorie' => 'Ligne de trésorie',
                    'Court terme' => 'Court terme',
                    'Moyen terme' => 'Moyen terme',
                ]
            ])

            ->add('objet', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Simulation::class,
        ]);
    }
}
