<?php

namespace App\Form\ExerciceCp;

use App\Entity\ExerciceCp\SimulationFinancement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimulationFinancementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('libelle', ChoiceType::class, [
                'label' => 'Type de simulation',
                'placeholder' => '-- Choisissez --',
                'choices' => [
                    'Credit 1' => 'Credit 1',
                    'Credit 2' => 'Credit 2',
                    'Credit 3' => 'Credit 3',
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SimulationFinancement::class,
        ]);
    }
}
