<?php

namespace App\Form\ExerciceCp;

use App\Entity\ExerciceCp\SimulationDuree;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinancementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('offre', SimulationOffreType::class, [
                'label' => false,
            ])
            ->add('simulation', SimulationType::class, [
                'label' => false,
            ])
            ->add('durees', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => SimulationDureeType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])

            ->add('montant', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => SimulationMontantType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('financements', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => SimulationFinancementType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here

        ]);
    }
}
