<?php

namespace App\Form;

use App\Entity\Annonces;
use App\Entity\Departements;
use App\Entity\Regions;
use App\Entity\VillesFrance;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnoncesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class,)
            ->add('content', TextareaType::class)
            ->add('active')

            ->add('regions', EntityType::class, [
                'mapped' => false,
                'class' => Regions::class,
                'choice_label' => 'name',
                'placeholder' => 'Région ( -- Choisissez -- )',
                'label' => 'Région',
                'required' => false
            ])
            ->add('departements', ChoiceType::class, [
                'placeholder' => 'Département (Choisir une région)',
                'required' => false
            ])
            ->add('ville', ChoiceType::class, [
                'placeholder' => 'Ville (Choisir un département)',
                'required' => false
            ])
            ->add('Valider', SubmitType::class)
        ;
        $formModifier = function (FormInterface $form, Regions $regions = null) {
            $departements = null === $regions ? [] : $regions->getDepartements();
            $form->add('departements', EntityType::class, [
                'class' => Departements::class,
                'choices' => $departements,
                'required' => false,
                'choice_label' => 'name',
                'placeholder' => 'Département (Choisir une région)',
                'attr' => ['class' => 'custom-select'],
                'label' => 'Département'
            ]);
        };

        $formModifierVille = function (FormInterface $form, Departements $departements = null) {
            $villes = null === $departements ? [] : $departements->getVillesFrances();
            $form->add('ville', EntityType::class, [
                'class' => VillesFrance::class,
                'choices' => $villes,
                'required' => false,
                'choice_label' => 'ville_nom_reel',
                'placeholder' => 'Villes (Choisir un département)',
                'attr' => ['class' => 'custom-select'],
                'label' => 'Ville '
            ]);
        };
        $builder->get('regions')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $region = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $region);
            }
        );
        $builder->get('departements')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifierVille) {
                $departement = $event->getForm()->getData();
                $formModifierVille($event->getForm()->getParent(), $departement);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Annonces::class,
        ]);
    }
}
