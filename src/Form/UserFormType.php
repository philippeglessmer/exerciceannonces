<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\ExerciceCp\User\ColGenre;
use App\Entity\ExerciceCp\User\ColFonction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('genre', EntityType::class, [
                'class' => ColGenre::class,
                'choice_label' => 'libelle',
                'placeholder' => 'Sélectionnez un genre',
                'required' => true,
                'mapped' => false,
            ])
//            ->add('fonction', EntityType::class, [
//                'class' => ColFonction::class,
//                'choice_label' => 'libelle',
//                'placeholder' => 'Sélectionnez une fonction',
//                'required' => true,
//            ])
            ->add('fonction', ChoiceType::class, [
                'placeholder' => 'Sélectionnez une fonction',
                'required' => true,
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();

                if (isset($data['genre'])) {
                    $genre = $data['genre'];
                    $form->add('fonction', EntityType::class, [
                        'class' => ColFonction::class,
                        'choice_label' => 'libelle',
                        'placeholder' => 'Sélectionnez une fonction',
                        'required' => true,
                        'query_builder' => function ($repository) use ($genre) {
                            return $repository->createQueryBuilder('f')
                                ->join('f.genre', 'g')
                                ->where('g.libelle = :genre')
                                ->setParameter('genre', $genre);
                        },
                    ]);
                }
            });
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
