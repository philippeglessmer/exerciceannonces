<?php

namespace App\Service;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
class FormatagesExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('truncate', [$this, 'truncateString']),
        ];
    }

    public function truncateString($string, $length)
    {
        if (mb_strlen($string) > $length) {
            $truncatedString = mb_substr($string, 0, $length - 3) . '...';
        } else {
            $truncatedString = $string;
        }

        return $truncatedString;
    }
}