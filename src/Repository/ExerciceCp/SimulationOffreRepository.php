<?php

namespace App\Repository\ExerciceCp;

use App\Entity\ExerciceCp\Collectivite;
use App\Entity\ExerciceCp\SimulationOffre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SimulationOffre>
 *
 * @method SimulationOffre|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimulationOffre|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimulationOffre[]    findAll()
 * @method SimulationOffre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimulationOffreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SimulationOffre::class);
    }

    public function save(SimulationOffre $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SimulationOffre $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function saveFromFinancement($data, $Collectivite = null) : SimulationOffre
    {
        $offre = new SimulationOffre();
        $offre->setNumOffre($data->getNumOffre());
        $offre->setCollectivite($Collectivite);
        $this->save($offre, true);
        return $offre;
    }
//    /**
//     * @return SimulationOffre[] Returns an array of SimulationOffre objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SimulationOffre
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
    public function camelCaseToSnakeCase($input) {
        $pattern = '/(?<=[a-z])([A-Z])/';
        $replacement = '_$1';
        $snakeCase = preg_replace($pattern, $replacement, $input);
        return strtolower($snakeCase);
    }
}
