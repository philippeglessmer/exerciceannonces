<?php

namespace App\Repository\ExerciceCp;

use App\Entity\ExerciceCp\Simulation;
use App\Entity\ExerciceCp\SimulationFinancement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SimulationFinancement>
 *
 * @method SimulationFinancement|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimulationFinancement|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimulationFinancement[]    findAll()
 * @method SimulationFinancement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimulationFinancementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SimulationFinancement::class);
    }

    public function save(SimulationFinancement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SimulationFinancement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function saveFromFinancement(array $data, Simulation $simulation) : bool
    {
        $this->deleteNullSimulationFinancement();
        foreach ($data as $key => $value) {
            $financement = new SimulationFinancement();
            $financement->setSimulation($simulation);
            $financement->setLibelle($value->getLibelle());
            $this->save($financement, true);
        }
        return true;
    }
    public function deleteNullSimulationFinancement(): void
    {
        $qb = $this->createQueryBuilder('d');
        $qb->delete()
            ->where($qb->expr()->isNull('d.simulation'));

        $query = $qb->getQuery();
        $query->execute();
    }
//    /**
//     * @return SimulationFinancement[] Returns an array of SimulationFinancement objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SimulationFinancement
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
