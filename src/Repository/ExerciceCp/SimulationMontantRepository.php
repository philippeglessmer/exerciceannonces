<?php

namespace App\Repository\ExerciceCp;

use App\Entity\ExerciceCp\Simulation;
use App\Entity\ExerciceCp\SimulationMontant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SimulationMontant>
 *
 * @method SimulationMontant|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimulationMontant|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimulationMontant[]    findAll()
 * @method SimulationMontant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimulationMontantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SimulationMontant::class);
    }

    public function save(SimulationMontant $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SimulationMontant $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function saveFromFinancement(array $data, Simulation $simulation) : bool
    {
        $this->deleteNullSimulationMontant();
        foreach ($data as $key => $value) {
            $montant = new SimulationMontant();
            $montant->setMontant($value->getMontant());
            $montant->setFrais($value->getFrais());
            $montant->setSimulation($simulation);
            $this->save($montant, true);
        }
        return true;
    }
    public function deleteNullSimulationMontant(): void
    {
        $qb = $this->createQueryBuilder('d');
        $qb->delete()
            ->where($qb->expr()->isNull('d.simulation'));

        $query = $qb->getQuery();
        $query->execute();
    }
//    /**
//     * @return SimulationMontant[] Returns an array of SimulationMontant objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SimulationMontant
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
