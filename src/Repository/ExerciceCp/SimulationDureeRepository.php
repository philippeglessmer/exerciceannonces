<?php

namespace App\Repository\ExerciceCp;

use App\Entity\ExerciceCp\Simulation;
use App\Entity\ExerciceCp\SimulationDuree;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SimulationDuree>
 *
 * @method SimulationDuree|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimulationDuree|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimulationDuree[]    findAll()
 * @method SimulationDuree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimulationDureeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SimulationDuree::class);
    }

    public function save(SimulationDuree $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SimulationDuree $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function saveFromFinancement(array $data, Simulation $simulation) : bool
    {
        $this->deleteNullSimulationDuree();
        foreach ($data as $key => $value) {
            $duree = new SimulationDuree();
            $duree->setDuree($value->getDuree());
            $duree->setSimulation($simulation);
            $this->save($duree, true);

        }
        return true;
    }
    public function deleteNullSimulationDurees()
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery('DELETE FROM App\Entity\ExerciceCp\SimulationDuree d WHERE d.simulation IS NULL');
        return $query->execute();
    }
    public function deleteNullSimulationDuree(): void
    {
        $qb = $this->createQueryBuilder('d');
        $qb->delete()
            ->where($qb->expr()->isNull('d.simulation'));

        $query = $qb->getQuery();
        $query->execute();
    }
//    /**
//     * @return SimulationDuree[] Returns an array of SimulationDuree objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SimulationDuree
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
