<?php

namespace App\Repository\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurIdentification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ActeurIdentification>
 *
 * @method ActeurIdentification|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActeurIdentification|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActeurIdentification[]    findAll()
 * @method ActeurIdentification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActeurIdentificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActeurIdentification::class);
    }

    public function save(ActeurIdentification $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ActeurIdentification $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ActeurIdentification[] Returns an array of ActeurIdentification objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ActeurIdentification
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
