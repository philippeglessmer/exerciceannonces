<?php

namespace App\Repository\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurMembre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ActeurMembre>
 *
 * @method ActeurMembre|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActeurMembre|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActeurMembre[]    findAll()
 * @method ActeurMembre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActeurMembreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActeurMembre::class);
    }

    public function save(ActeurMembre $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ActeurMembre $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ActeurMembre[] Returns an array of ActeurMembre objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ActeurMembre
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
