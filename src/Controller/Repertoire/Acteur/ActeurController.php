<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\Acteur;
use App\Form\Repertoire\Acteur\ActeurType;
use App\Repository\Repertoire\Acteur\ActeurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/acteur')]
class ActeurController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_index', methods: ['GET'])]
    public function index(ActeurRepository $acteurRepository): Response
    {
        return $this->render('repertoire/acteur/acteur/index.html.twig', [
            'acteurs' => $acteurRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurRepository $acteurRepository): Response
    {
        $acteur = new Acteur();
        $form = $this->createForm(ActeurType::class, $acteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurRepository->save($acteur, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('repertoire/acteur/acteur/new.html.twig', [
            'acteur' => $acteur,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_show', methods: ['GET'])]
    public function show(Acteur $acteur): Response
    {
        return $this->render('repertoire/acteur/acteur/show.html.twig', [
            'acteur' => $acteur,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Acteur $acteur, ActeurRepository $acteurRepository): Response
    {
        $form = $this->createForm(ActeurType::class, $acteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurRepository->save($acteur, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur/edit.html.twig', [
            'acteur' => $acteur,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_delete', methods: ['POST'])]
    public function delete(Request $request, Acteur $acteur, ActeurRepository $acteurRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteur->getId(), $request->request->get('_token'))) {
            $acteurRepository->remove($acteur, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_index', [], Response::HTTP_SEE_OTHER);
    }
}
