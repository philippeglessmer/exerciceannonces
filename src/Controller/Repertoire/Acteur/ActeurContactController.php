<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurContact;
use App\Form\Repertoire\Acteur\ActeurContactType;
use App\Repository\Repertoire\Acteur\ActeurContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/acteur/contact')]
class ActeurContactController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_contact_index', methods: ['GET'])]
    public function index(ActeurContactRepository $acteurContactRepository): Response
    {
        return $this->render('repertoire/acteur/acteur_contact/index.html.twig', [
            'acteur_contacts' => $acteurContactRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_contact_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurContactRepository $acteurContactRepository): Response
    {
        $acteurContact = new ActeurContact();
        $form = $this->createForm(ActeurContactType::class, $acteurContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurContactRepository->save($acteurContact, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_contact/new.html.twig', [
            'acteur_contact' => $acteurContact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_contact_show', methods: ['GET'])]
    public function show(ActeurContact $acteurContact): Response
    {
        return $this->render('repertoire/acteur/acteur_contact/show.html.twig', [
            'acteur_contact' => $acteurContact,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_contact_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActeurContact $acteurContact, ActeurContactRepository $acteurContactRepository): Response
    {
        $form = $this->createForm(ActeurContactType::class, $acteurContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurContactRepository->save($acteurContact, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_contact/edit.html.twig', [
            'acteur_contact' => $acteurContact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_contact_delete', methods: ['POST'])]
    public function delete(Request $request, ActeurContact $acteurContact, ActeurContactRepository $acteurContactRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteurContact->getId(), $request->request->get('_token'))) {
            $acteurContactRepository->remove($acteurContact, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_contact_index', [], Response::HTTP_SEE_OTHER);
    }
}
