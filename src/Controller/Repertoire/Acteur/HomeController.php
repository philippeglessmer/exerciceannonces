<?php

namespace App\Controller\Repertoire\Acteur;

use App\Form\Repertoire\Acteur\ActeurAddType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/repertoire/acteur/', name: 'app_repertoire_acteur_home')]
    public function index(): Response
    {
        return $this->render('repertoire/acteur/home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    #[Route('/repertoire/acteur/add-acteur', name: 'app_repertoire_acteur_add')]
    public function add(Request $request,): Response
    {

        $form = $this->createForm(ActeurAddType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dd($form->getData());
//            return $this->redirectToRoute('app_repertoire_acteur_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('repertoire/acteur/home/new.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView()
        ]);
    }
}
