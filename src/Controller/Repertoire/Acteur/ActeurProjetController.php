<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurProjet;
use App\Form\Repertoire\Acteur\ActeurProjetType;
use App\Repository\Repertoire\Acteur\ActeurProjetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/acteur/projet')]
class ActeurProjetController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_projet_index', methods: ['GET'])]
    public function index(ActeurProjetRepository $acteurProjetRepository): Response
    {
        return $this->render('repertoire/acteur/acteur_projet/index.html.twig', [
            'acteur_projets' => $acteurProjetRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_projet_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurProjetRepository $acteurProjetRepository): Response
    {
        $acteurProjet = new ActeurProjet();
        $form = $this->createForm(ActeurProjetType::class, $acteurProjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurProjetRepository->save($acteurProjet, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_projet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_projet/new.html.twig', [
            'acteur_projet' => $acteurProjet,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_projet_show', methods: ['GET'])]
    public function show(ActeurProjet $acteurProjet): Response
    {
        return $this->render('repertoire/acteur/acteur_projet/show.html.twig', [
            'acteur_projet' => $acteurProjet,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_projet_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActeurProjet $acteurProjet, ActeurProjetRepository $acteurProjetRepository): Response
    {
        $form = $this->createForm(ActeurProjetType::class, $acteurProjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurProjetRepository->save($acteurProjet, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_projet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_projet/edit.html.twig', [
            'acteur_projet' => $acteurProjet,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_projet_delete', methods: ['POST'])]
    public function delete(Request $request, ActeurProjet $acteurProjet, ActeurProjetRepository $acteurProjetRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteurProjet->getId(), $request->request->get('_token'))) {
            $acteurProjetRepository->remove($acteurProjet, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_projet_index', [], Response::HTTP_SEE_OTHER);
    }
}
