<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurMembre;
use App\Form\Repertoire\Acteur\ActeurMembreType;
use App\Repository\Repertoire\Acteur\ActeurMembreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/membre')]
class ActeurMembreController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_membre_index', methods: ['GET'])]
    public function index(ActeurMembreRepository $acteurMembreRepository): Response
    {
        return $this->render('repertoire/acteur/acteur_membre/index.html.twig', [
            'acteur_membres' => $acteurMembreRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_membre_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurMembreRepository $acteurMembreRepository): Response
    {
        $acteurMembre = new ActeurMembre();
        $form = $this->createForm(ActeurMembreType::class, $acteurMembre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurMembreRepository->save($acteurMembre, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_membre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_membre/new.html.twig', [
            'acteur_membre' => $acteurMembre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_membre_show', methods: ['GET'])]
    public function show(ActeurMembre $acteurMembre): Response
    {
        return $this->render('repertoire/acteur/acteur_membre/show.html.twig', [
            'acteur_membre' => $acteurMembre,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_membre_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActeurMembre $acteurMembre, ActeurMembreRepository $acteurMembreRepository): Response
    {
        $form = $this->createForm(ActeurMembreType::class, $acteurMembre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurMembreRepository->save($acteurMembre, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_membre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_membre/edit.html.twig', [
            'acteur_membre' => $acteurMembre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_membre_delete', methods: ['POST'])]
    public function delete(Request $request, ActeurMembre $acteurMembre, ActeurMembreRepository $acteurMembreRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteurMembre->getId(), $request->request->get('_token'))) {
            $acteurMembreRepository->remove($acteurMembre, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_membre_index', [], Response::HTTP_SEE_OTHER);
    }
}
