<?php

namespace App\Controller\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\Lib\Fonctions;
use App\Form\Repertoire\Acteur\Lib\FonctionsType;
use App\Repository\Repertoire\Acteur\Lib\FonctionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/lib/fonctions')]
class FonctionsController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_lib_fonctions_index', methods: ['GET'])]
    public function index(FonctionsRepository $fonctionsRepository): Response
    {
        return $this->render('repertoire/acteur/lib/fonctions/index.html.twig', [
            'fonctions' => $fonctionsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_lib_fonctions_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FonctionsRepository $fonctionsRepository): Response
    {
        $fonction = new Fonctions();
        $form = $this->createForm(FonctionsType::class, $fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fonctionsRepository->save($fonction, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_fonctions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/fonctions/new.html.twig', [
            'fonction' => $fonction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_fonctions_show', methods: ['GET'])]
    public function show(Fonctions $fonction): Response
    {
        return $this->render('repertoire/acteur/lib/fonctions/show.html.twig', [
            'fonction' => $fonction,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_lib_fonctions_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Fonctions $fonction, FonctionsRepository $fonctionsRepository): Response
    {
        $form = $this->createForm(FonctionsType::class, $fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fonctionsRepository->save($fonction, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_fonctions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/fonctions/edit.html.twig', [
            'fonction' => $fonction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_fonctions_delete', methods: ['POST'])]
    public function delete(Request $request, Fonctions $fonction, FonctionsRepository $fonctionsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fonction->getId(), $request->request->get('_token'))) {
            $fonctionsRepository->remove($fonction, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_lib_fonctions_index', [], Response::HTTP_SEE_OTHER);
    }
}
