<?php

namespace App\Controller\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\Lib\Categorie;
use App\Form\Repertoire\Acteur\Lib\CategorieType;
use App\Repository\Repertoire\Acteur\Lib\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/lib/categorie')]
class CategorieController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_lib_categorie_index', methods: ['GET'])]
    public function index(CategorieRepository $categorieRepository): Response
    {
        return $this->render('repertoire/acteur/lib/categorie/index.html.twig', [
            'categories' => $categorieRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_lib_categorie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategorieRepository $categorieRepository): Response
    {
        $categorie = new Categorie();
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categorieRepository->save($categorie, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/categorie/new.html.twig', [
            'categorie' => $categorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_categorie_show', methods: ['GET'])]
    public function show(Categorie $categorie): Response
    {
        return $this->render('repertoire/acteur/lib/categorie/show.html.twig', [
            'categorie' => $categorie,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_lib_categorie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Categorie $categorie, CategorieRepository $categorieRepository): Response
    {
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categorieRepository->save($categorie, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/categorie/edit.html.twig', [
            'categorie' => $categorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_categorie_delete', methods: ['POST'])]
    public function delete(Request $request, Categorie $categorie, CategorieRepository $categorieRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorie->getId(), $request->request->get('_token'))) {
            $categorieRepository->remove($categorie, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_lib_categorie_index', [], Response::HTTP_SEE_OTHER);
    }
}
