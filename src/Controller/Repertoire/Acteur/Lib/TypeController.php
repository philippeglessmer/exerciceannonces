<?php

namespace App\Controller\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\Lib\Type;
use App\Form\Repertoire\Acteur\Lib\TypeType;
use App\Repository\Repertoire\Acteur\Lib\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/lib/type')]
class TypeController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_lib_type_index', methods: ['GET'])]
    public function index(TypeRepository $typeRepository): Response
    {
        return $this->render('repertoire/acteur/lib/type/index.html.twig', [
            'types' => $typeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_lib_type_new', methods: ['GET', 'POST'])]
    public function new(Request $request, TypeRepository $typeRepository): Response
    {
        $type = new Type();
        $form = $this->createForm(TypeType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeRepository->save($type, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/type/new.html.twig', [
            'type' => $type,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_type_show', methods: ['GET'])]
    public function show(Type $type): Response
    {
        return $this->render('repertoire/acteur/lib/type/show.html.twig', [
            'type' => $type,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_lib_type_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Type $type, TypeRepository $typeRepository): Response
    {
        $form = $this->createForm(TypeType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeRepository->save($type, true);

            return $this->redirectToRoute('app_repertoire_acteur_lib_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/lib/type/edit.html.twig', [
            'type' => $type,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_lib_type_delete', methods: ['POST'])]
    public function delete(Request $request, Type $type, TypeRepository $typeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$type->getId(), $request->request->get('_token'))) {
            $typeRepository->remove($type, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_lib_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
