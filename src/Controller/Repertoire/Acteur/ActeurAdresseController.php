<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurAdresse;
use App\Form\Repertoire\Acteur\ActeurAdresseType;
use App\Repository\Repertoire\Acteur\ActeurAdresseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/acteur/adresse')]
class ActeurAdresseController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_adresse_index', methods: ['GET'])]
    public function index(ActeurAdresseRepository $acteurAdresseRepository): Response
    {
        return $this->render('repertoire/acteur/acteur_adresse/index.html.twig', [
            'acteur_adresses' => $acteurAdresseRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_adresse_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurAdresseRepository $acteurAdresseRepository): Response
    {
        $acteurAdresse = new ActeurAdresse();
        $form = $this->createForm(ActeurAdresseType::class, $acteurAdresse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurAdresseRepository->save($acteurAdresse, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_adresse_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_adresse/new.html.twig', [
            'acteur_adresse' => $acteurAdresse,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_adresse_show', methods: ['GET'])]
    public function show(ActeurAdresse $acteurAdresse): Response
    {
        return $this->render('repertoire/acteur/acteur_adresse/show.html.twig', [
            'acteur_adresse' => $acteurAdresse,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_adresse_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActeurAdresse $acteurAdresse, ActeurAdresseRepository $acteurAdresseRepository): Response
    {
        $form = $this->createForm(ActeurAdresseType::class, $acteurAdresse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurAdresseRepository->save($acteurAdresse, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_adresse_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_adresse/edit.html.twig', [
            'acteur_adresse' => $acteurAdresse,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_adresse_delete', methods: ['POST'])]
    public function delete(Request $request, ActeurAdresse $acteurAdresse, ActeurAdresseRepository $acteurAdresseRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteurAdresse->getId(), $request->request->get('_token'))) {
            $acteurAdresseRepository->remove($acteurAdresse, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_adresse_index', [], Response::HTTP_SEE_OTHER);
    }
}
