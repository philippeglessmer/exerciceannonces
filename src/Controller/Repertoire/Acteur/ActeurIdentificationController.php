<?php

namespace App\Controller\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\ActeurIdentification;
use App\Form\Repertoire\Acteur\ActeurIdentificationType;
use App\Repository\Repertoire\Acteur\ActeurIdentificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/repertoire/acteur/acteur/identification')]
class ActeurIdentificationController extends AbstractController
{
    #[Route('/', name: 'app_repertoire_acteur_acteur_identification_index', methods: ['GET'])]
    public function index(ActeurIdentificationRepository $acteurIdentificationRepository): Response
    {
        return $this->render('repertoire/acteur/acteur_identification/index.html.twig', [
            'acteur_identifications' => $acteurIdentificationRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_repertoire_acteur_acteur_identification_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ActeurIdentificationRepository $acteurIdentificationRepository): Response
    {
        $acteurIdentification = new ActeurIdentification();
        $form = $this->createForm(ActeurIdentificationType::class, $acteurIdentification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurIdentificationRepository->save($acteurIdentification, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_identification_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_identification/new.html.twig', [
            'acteur_identification' => $acteurIdentification,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_identification_show', methods: ['GET'])]
    public function show(ActeurIdentification $acteurIdentification): Response
    {
        return $this->render('repertoire/acteur/acteur_identification/show.html.twig', [
            'acteur_identification' => $acteurIdentification,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_repertoire_acteur_acteur_identification_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ActeurIdentification $acteurIdentification, ActeurIdentificationRepository $acteurIdentificationRepository): Response
    {
        $form = $this->createForm(ActeurIdentificationType::class, $acteurIdentification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acteurIdentificationRepository->save($acteurIdentification, true);

            return $this->redirectToRoute('app_repertoire_acteur_acteur_identification_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('repertoire/acteur/acteur_identification/edit.html.twig', [
            'acteur_identification' => $acteurIdentification,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_repertoire_acteur_acteur_identification_delete', methods: ['POST'])]
    public function delete(Request $request, ActeurIdentification $acteurIdentification, ActeurIdentificationRepository $acteurIdentificationRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$acteurIdentification->getId(), $request->request->get('_token'))) {
            $acteurIdentificationRepository->remove($acteurIdentification, true);
        }

        return $this->redirectToRoute('app_repertoire_acteur_acteur_identification_index', [], Response::HTTP_SEE_OTHER);
    }
}
