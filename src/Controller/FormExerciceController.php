<?php

namespace App\Controller;

use App\Form\AddChampType;
use App\Form\AddImagesType;
use App\Service\PictureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FormExerciceController extends AbstractController
{
    #[Route('/form/exercice', name: 'app_form_exercice')]
    public function index(): Response
    {
        return $this->render('form_exercice/index.html.twig', [
            'controller_name' => 'FormExerciceController',
        ]);
    }
    #[Route('/form/exercice1', name: 'app_form_exercice1')]
    public function index1(Request $request): Response
    {
        $form = $this->createForm(AddChampType::class);
        $form->handleRequest($request);
        return $this->render('form_exercice/new.html.twig', [
            'controller_name' => 'FormExerciceController',
            'form' => $form->createView(),
        ]);
    }
    #[Route('/form/exercice2', name: 'app_form_exercice2')]
    public function index2(Request $request, PictureService $pictureService): Response
    {
        $form = $this->createForm(AddImagesType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

        $images = $form->get('images')->getData();
            foreach($images as $image){
                $folder = 'products';
                $images = $pictureService->add($image, $folder, 300, 300);
            }
//            return $this->redirectToRoute('app_form_exercice2', [], Response::HTTP_SEE_OTHER);
        }


        return $this->render('form_exercice/form.html.twig', [
            'controller_name' => 'FormExerciceController',
            'form' => $form->createView(),
        ]);
    }
}
