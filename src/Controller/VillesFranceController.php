<?php

namespace App\Controller;

use App\Entity\VillesFrance;
use App\Form\VillesFranceType;
use App\Repository\VillesFranceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/villes/france')]
class VillesFranceController extends AbstractController
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'app_villes_france_index', methods: ['GET'])]
    public function index(Request $request, PaginatorInterface $paginator, VillesFranceRepository $villesFranceRepository): Response
    {
        $donnes = $this->entityManager->getRepository(VillesFrance::class)->findBy([], ['ville_nom_reel' => 'ASC']);
        $villes = $paginator->paginate(
            $donnes,
            $request->query->getInt('page', 1),
            25
        );
        return $this->render('villes_france/index.html.twig', [
            'villes_frances' => $villes,

        ]);
    }

    #[Route('/new', name: 'app_villes_france_new', methods: ['GET', 'POST'])]
    public function new(Request $request, VillesFranceRepository $villesFranceRepository): Response
    {
        $villesFrance = new VillesFrance();
        $form = $this->createForm(VillesFranceType::class, $villesFrance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $villesFranceRepository->save($villesFrance, true);

            return $this->redirectToRoute('app_villes_france_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('villes_france/new.html.twig', [
            'villes_france' => $villesFrance,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_villes_france_show', methods: ['GET'])]
    public function show(VillesFrance $villesFrance): Response
    {
        return $this->render('villes_france/show.html.twig', [
            'villes_france' => $villesFrance,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_villes_france_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, VillesFrance $villesFrance, VillesFranceRepository $villesFranceRepository): Response
    {
        $form = $this->createForm(VillesFranceType::class, $villesFrance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $villesFranceRepository->save($villesFrance, true);

            return $this->redirectToRoute('app_villes_france_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('villes_france/edit.html.twig', [
            'villes_france' => $villesFrance,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_villes_france_delete', methods: ['POST'])]
    public function delete(Request $request, VillesFrance $villesFrance, VillesFranceRepository $villesFranceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$villesFrance->getId(), $request->request->get('_token'))) {
            $villesFranceRepository->remove($villesFrance, true);
        }

        return $this->redirectToRoute('app_villes_france_index', [], Response::HTTP_SEE_OTHER);
    }
}
