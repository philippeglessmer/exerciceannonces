<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\Collectivite;
use App\Entity\ExerciceCp\Simulation;
use App\Entity\ExerciceCp\SimulationOffre;
use App\Form\ExerciceCp\FinancementType;
use App\Repository\ExerciceCp\SimulationDureeRepository;
use App\Repository\ExerciceCp\SimulationFinancementRepository;
use App\Repository\ExerciceCp\SimulationMontantRepository;
use App\Repository\ExerciceCp\SimulationOffreRepository;
use App\Repository\ExerciceCp\SimulationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FinancementController extends AbstractController
{
    private $offreRepository;
    private $simulationRepository;
    private $dureeRepository;
    private $montantRepository;

    private $financementRepository;
    public function __construct(SimulationOffreRepository $offreRepository,SimulationRepository $simulationRepository, SimulationDureeRepository $dureeRepository, SimulationMontantRepository $montantRepository, SimulationFinancementRepository $financementRepository){
        $this->offreRepository = $offreRepository;
        $this->simulationRepository = $simulationRepository;
        $this->dureeRepository = $dureeRepository;
        $this->montantRepository = $montantRepository;
        $this->financementRepository = $financementRepository;
    }

    #[Route('/exercice/financement/collectivite/{collectivite}', name: 'app_exercice_cp_financement')]
    public function index(Request $request, Collectivite $collectivite): Response
    {
        $form = $this->createForm(FinancementType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->get('offre')->getData()->setNumOffre(uniqid());

            $offre = $this->offreRepository->saveFromFinancement($form->get('offre')->getData(), $collectivite);

            $simulationId =   $this->simulationRepository->saveFromFinancement($form->get('simulation')->getData(), $offre);
            $simulation = $this->simulationRepository->find($simulationId);
            $durees = $this->dureeRepository->saveFromFinancement($form->get('durees')->getData(), $simulation);
            $montants = $this->montantRepository->saveFromFinancement($form->get('montant')->getData(), $simulation);
            $financements =  $this->financementRepository->saveFromFinancement($form->get('financements')->getData(), $simulation);
            $this->addFlash('success', 'L\'enregistrement s\'est bien passé');
            return $this->redirectToRoute('app_exercice_cp_financement_simulation', [ 'simulation' => $simulationId ], Response::HTTP_SEE_OTHER);
        }
        return $this->render('exercice_cp/financement/index.html.twig', [
            'controller_name' => 'FinancementController',
            'form' => $form->createView(),
            'simulations' => null,
            'new' => null,

        ]);

    }
    #[Route('/exercice/financement/offre/{simulationOffre}', name: 'app_exercice_cp_financement_offre')]
    public function offre(Request $request, SimulationOffre $simulationOffre): Response
    {

        $form = $this->createForm(FinancementType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $simulationId =   $this->simulationRepository->saveFromFinancement($form->get('simulation')->getData(), $simulationOffre);
                $simulation = $this->simulationRepository->find($simulationId);
                $durees = $this->dureeRepository->saveFromFinancement($form->get('durees')->getData(), $simulation);
                $montants = $this->montantRepository->saveFromFinancement($form->get('montant')->getData(), $simulation);
                $financements =  $this->financementRepository->saveFromFinancement($form->get('financements')->getData(), $simulation);
                $this->addFlash('success', 'L\'enregistrement s\'est bien passé');
            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }

            return $this->redirectToRoute('app_exercice_cp_financement_simulation', [ 'simulation' => $simulationId ], Response::HTTP_SEE_OTHER);
        }
        return $this->render('exercice_cp/financement/index.html.twig', [
            'controller_name' => 'FinancementController',
            'form' => $form->createView(),
            'simulations' => $simulationOffre->getSimulations(),
            'new' => $simulationOffre->getId(),
        ]);

    }
    #[Route('/exercice/financement/simulation/{simulation}', name: 'app_exercice_cp_financement_simulation')]
    public function simulation(Request $request, Simulation $simulation): Response
    {
        $form = $this->createForm(FinancementType::class, $simulation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->simulationRepository->save($simulation, true);
            $this->addFlash('success', 'La modification s\'est bien passé');
            return $this->redirectToRoute('app_exercice_cp_financement_simulation', [ 'simulation' => $simulation->getId() ], Response::HTTP_SEE_OTHER);
        }
        return $this->render('exercice_cp/financement/index.html.twig', [
            'controller_name' => 'FinancementController',
            'form' => $form->createView(),
            'simulations' => $simulation->getOffre()->getSimulations(),
            'new' => $simulation->getOffre()->getId(),
        ]);

    }
}
//https://aymeric-cucherousset.fr/symfony-6-flash-messages/
