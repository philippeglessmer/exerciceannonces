<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\SimulationOffre;
use App\Form\ExerciceCp\SimulationOffreType;
use App\Repository\ExerciceCp\SimulationOffreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/simulation/offre')]
class SimulationOffreController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_simulation_offre_index', methods: ['GET'])]
    public function index(SimulationOffreRepository $simulationOffreRepository): Response
    {
        return $this->render('exercice_cp/simulation_offre/index.html.twig', [
            'simulation_offres' => $simulationOffreRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_simulation_offre_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimulationOffreRepository $simulationOffreRepository): Response
    {
        $simulationOffre = new SimulationOffre();
        $form = $this->createForm(SimulationOffreType::class, $simulationOffre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationOffreRepository->save($simulationOffre, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_offre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_offre/new.html.twig', [
            'simulation_offre' => $simulationOffre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_offre_show', methods: ['GET'])]
    public function show(SimulationOffre $simulationOffre): Response
    {
        return $this->render('exercice_cp/simulation_offre/show.html.twig', [
            'simulation_offre' => $simulationOffre,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_simulation_offre_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SimulationOffre $simulationOffre, SimulationOffreRepository $simulationOffreRepository): Response
    {
        $form = $this->createForm(SimulationOffreType::class, $simulationOffre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationOffreRepository->save($simulationOffre, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_offre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_offre/edit.html.twig', [
            'simulation_offre' => $simulationOffre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_offre_delete', methods: ['POST'])]
    public function delete(Request $request, SimulationOffre $simulationOffre, SimulationOffreRepository $simulationOffreRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simulationOffre->getId(), $request->request->get('_token'))) {
            $simulationOffreRepository->remove($simulationOffre, true);
        }

        return $this->redirectToRoute('app_exercice_cp_simulation_offre_index', [], Response::HTTP_SEE_OTHER);
    }
}
