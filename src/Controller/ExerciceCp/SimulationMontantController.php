<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\SimulationMontant;
use App\Form\ExerciceCp\SimulationMontantType;
use App\Repository\ExerciceCp\SimulationMontantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/simulation/montant')]
class SimulationMontantController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_simulation_montant_index', methods: ['GET'])]
    public function index(SimulationMontantRepository $simulationMontantRepository): Response
    {
        return $this->render('exercice_cp/simulation_montant/index.html.twig', [
            'simulation_montants' => $simulationMontantRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_simulation_montant_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimulationMontantRepository $simulationMontantRepository): Response
    {
        $simulationMontant = new SimulationMontant();
        $form = $this->createForm(SimulationMontantType::class, $simulationMontant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationMontantRepository->save($simulationMontant, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_montant_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_montant/new.html.twig', [
            'simulation_montant' => $simulationMontant,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_montant_show', methods: ['GET'])]
    public function show(SimulationMontant $simulationMontant): Response
    {
        return $this->render('exercice_cp/simulation_montant/show.html.twig', [
            'simulation_montant' => $simulationMontant,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_simulation_montant_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SimulationMontant $simulationMontant, SimulationMontantRepository $simulationMontantRepository): Response
    {
        $form = $this->createForm(SimulationMontantType::class, $simulationMontant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationMontantRepository->save($simulationMontant, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_montant_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_montant/edit.html.twig', [
            'simulation_montant' => $simulationMontant,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_montant_delete', methods: ['POST'])]
    public function delete(Request $request, SimulationMontant $simulationMontant, SimulationMontantRepository $simulationMontantRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simulationMontant->getId(), $request->request->get('_token'))) {
            $simulationMontantRepository->remove($simulationMontant, true);
        }

        return $this->redirectToRoute('app_exercice_cp_simulation_montant_index', [], Response::HTTP_SEE_OTHER);
    }
}
