<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\Simulation;
use App\Form\ExerciceCp\SimulationType;
use App\Repository\ExerciceCp\SimulationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/simulation')]
class SimulationController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_simulation_index', methods: ['GET'])]
    public function index(SimulationRepository $simulationRepository): Response
    {
        return $this->render('exercice_cp/simulation/index.html.twig', [
            'simulations' => $simulationRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_simulation_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimulationRepository $simulationRepository): Response
    {
        $simulation = new Simulation();
        $form = $this->createForm(SimulationType::class, $simulation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationRepository->save($simulation, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation/new.html.twig', [
            'simulation' => $simulation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_show', methods: ['GET'])]
    public function show(Simulation $simulation): Response
    {
        return $this->render('exercice_cp/simulation/show.html.twig', [
            'simulation' => $simulation,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_simulation_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Simulation $simulation, SimulationRepository $simulationRepository): Response
    {
        $form = $this->createForm(SimulationType::class, $simulation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationRepository->save($simulation, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation/edit.html.twig', [
            'simulation' => $simulation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_delete', methods: ['POST'])]
    public function delete(Request $request, Simulation $simulation, SimulationRepository $simulationRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simulation->getId(), $request->request->get('_token'))) {
            $simulationRepository->remove($simulation, true);
        }

        return $this->redirectToRoute('app_exercice_cp_simulation_index', [], Response::HTTP_SEE_OTHER);
    }
}
