<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\SimulationFinancement;
use App\Form\ExerciceCp\SimulationFinancementType;
use App\Repository\ExerciceCp\SimulationFinancementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/simulation/financement')]
class SimulationFinancementController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_simulation_financement_index', methods: ['GET'])]
    public function index(SimulationFinancementRepository $simulationFinancementRepository): Response
    {
        return $this->render('exercice_cp/simulation_financement/index.html.twig', [
            'simulation_financements' => $simulationFinancementRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_simulation_financement_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimulationFinancementRepository $simulationFinancementRepository): Response
    {
        $simulationFinancement = new SimulationFinancement();
        $form = $this->createForm(SimulationFinancementType::class, $simulationFinancement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationFinancementRepository->save($simulationFinancement, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_financement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_financement/new.html.twig', [
            'simulation_financement' => $simulationFinancement,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_financement_show', methods: ['GET'])]
    public function show(SimulationFinancement $simulationFinancement): Response
    {
        return $this->render('exercice_cp/simulation_financement/show.html.twig', [
            'simulation_financement' => $simulationFinancement,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_simulation_financement_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SimulationFinancement $simulationFinancement, SimulationFinancementRepository $simulationFinancementRepository): Response
    {
        $form = $this->createForm(SimulationFinancementType::class, $simulationFinancement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationFinancementRepository->save($simulationFinancement, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_financement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_financement/edit.html.twig', [
            'simulation_financement' => $simulationFinancement,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_financement_delete', methods: ['POST'])]
    public function delete(Request $request, SimulationFinancement $simulationFinancement, SimulationFinancementRepository $simulationFinancementRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simulationFinancement->getId(), $request->request->get('_token'))) {
            $simulationFinancementRepository->remove($simulationFinancement, true);
        }

        return $this->redirectToRoute('app_exercice_cp_simulation_financement_index', [], Response::HTTP_SEE_OTHER);
    }
}
