<?php

namespace App\Controller\ExerciceCp\User;

use App\Entity\ExerciceCp\User\ColFonction;
use App\Entity\ExerciceCp\User\ColGenre;
use App\Form\ExerciceCp\User\ColGenreType;
use App\Form\UserFormType;
use App\Repository\ExerciceCp\User\ColGenreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/genre')]
class ColGenreController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'app_exercice_cp_user_col_genre_index', methods: ['GET'])]
    public function index(ColGenreRepository $colGenreRepository): Response
    {
        return $this->render('exercice_cp/user/col_genre/index.html.twig', [
            'col_genres' => $colGenreRepository->findAll(),
        ]);
    }
    #[Route('/test', name: 'app_exercice_cp_user_col_genre_test', methods: ['GET'])]
    public function test(ColGenreRepository $colGenreRepository): Response
    {
        $form = $this->createForm(UserFormType::class);
        return $this->render('exercice_cp/user/col_genre/test.html.twig', [

            'form' => $form,
        ]);
    }
    /**
     * @Route("/chemin-vers/action-ajax", name="action_ajax")
     */
    #[Route('/test-ajax', name: 'action_ajax')]
    public function actionAjax(Request $request)
    {
        $genreId = $request->query->get('genre');

        // Récupération du repository de l'entité Fonction
        $fonctionRepository = $this->entityManager->getRepository(ColFonction::class);

        // Récupération des fonctions en fonction du genre sélectionné
        $fonctions = $fonctionRepository->findBy(['genre' => $genreId]);

        // Formatage des fonctions pour la réponse JSON
        $formattedFonctions = [];
        foreach ($fonctions as $fonction) {
            $formattedFonctions[] = [
                'id' => $fonction->getId(),
                'libelle' => $fonction->getLibelle(),
            ];
        }

        // Renvoi des fonctions au format JSON
        return new JsonResponse($formattedFonctions);
    }
    #[Route('/new', name: 'app_exercice_cp_user_col_genre_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ColGenreRepository $colGenreRepository): Response
    {
        $colGenre = new ColGenre();
        $form = $this->createForm(ColGenreType::class, $colGenre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colGenreRepository->save($colGenre, true);

            return $this->redirectToRoute('app_exercice_cp_user_col_genre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/user/col_genre/new.html.twig', [
            'col_genre' => $colGenre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_user_col_genre_show', methods: ['GET'])]
    public function show(ColGenre $colGenre): Response
    {
        return $this->render('exercice_cp/user/col_genre/show.html.twig', [
            'col_genre' => $colGenre,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_user_col_genre_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ColGenre $colGenre, ColGenreRepository $colGenreRepository): Response
    {
        $form = $this->createForm(ColGenreType::class, $colGenre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colGenreRepository->save($colGenre, true);

            return $this->redirectToRoute('app_exercice_cp_user_col_genre_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/user/col_genre/edit.html.twig', [
            'col_genre' => $colGenre,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_user_col_genre_delete', methods: ['POST'])]
    public function delete(Request $request, ColGenre $colGenre, ColGenreRepository $colGenreRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$colGenre->getId(), $request->request->get('_token'))) {
            $colGenreRepository->remove($colGenre, true);
        }

        return $this->redirectToRoute('app_exercice_cp_user_col_genre_index', [], Response::HTTP_SEE_OTHER);
    }
}
