<?php

namespace App\Controller\ExerciceCp\User;

use App\Entity\ExerciceCp\User\ColFonction;
use App\Form\ExerciceCp\User\ColFonctionType;
use App\Repository\ExerciceCp\User\ColFonctionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/user/col/fonction')]
class ColFonctionController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_user_col_fonction_index', methods: ['GET'])]
    public function index(ColFonctionRepository $colFonctionRepository): Response
    {
        return $this->render('exercice_cp/user/col_fonction/index.html.twig', [
            'col_fonctions' => $colFonctionRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_user_col_fonction_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ColFonctionRepository $colFonctionRepository): Response
    {
        $colFonction = new ColFonction();
        $form = $this->createForm(ColFonctionType::class, $colFonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colFonctionRepository->save($colFonction, true);

            return $this->redirectToRoute('app_exercice_cp_user_col_fonction_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/user/col_fonction/new.html.twig', [
            'col_fonction' => $colFonction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_user_col_fonction_show', methods: ['GET'])]
    public function show(ColFonction $colFonction): Response
    {
        return $this->render('exercice_cp/user/col_fonction/show.html.twig', [
            'col_fonction' => $colFonction,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_user_col_fonction_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ColFonction $colFonction, ColFonctionRepository $colFonctionRepository): Response
    {
        $form = $this->createForm(ColFonctionType::class, $colFonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colFonctionRepository->save($colFonction, true);

            return $this->redirectToRoute('app_exercice_cp_user_col_fonction_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/user/col_fonction/edit.html.twig', [
            'col_fonction' => $colFonction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_user_col_fonction_delete', methods: ['POST'])]
    public function delete(Request $request, ColFonction $colFonction, ColFonctionRepository $colFonctionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$colFonction->getId(), $request->request->get('_token'))) {
            $colFonctionRepository->remove($colFonction, true);
        }

        return $this->redirectToRoute('app_exercice_cp_user_col_fonction_index', [], Response::HTTP_SEE_OTHER);
    }
}
