<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\SimulationDuree;
use App\Form\ExerciceCp\SimulationDureeType;
use App\Repository\ExerciceCp\SimulationDureeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/cp/simulation/duree')]
class SimulationDureeController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_simulation_duree_index', methods: ['GET'])]
    public function index(SimulationDureeRepository $simulationDureeRepository): Response
    {
        return $this->render('exercice_cp/simulation_duree/index.html.twig', [
            'simulation_durees' => $simulationDureeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_simulation_duree_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimulationDureeRepository $simulationDureeRepository): Response
    {
        $simulationDuree = new SimulationDuree();
        $form = $this->createForm(SimulationDureeType::class, $simulationDuree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationDureeRepository->save($simulationDuree, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_duree_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_duree/new.html.twig', [
            'simulation_duree' => $simulationDuree,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_duree_show', methods: ['GET'])]
    public function show(SimulationDuree $simulationDuree): Response
    {
        return $this->render('exercice_cp/simulation_duree/show.html.twig', [
            'simulation_duree' => $simulationDuree,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_simulation_duree_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SimulationDuree $simulationDuree, SimulationDureeRepository $simulationDureeRepository): Response
    {
        $form = $this->createForm(SimulationDureeType::class, $simulationDuree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simulationDureeRepository->save($simulationDuree, true);

            return $this->redirectToRoute('app_exercice_cp_simulation_duree_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/simulation_duree/edit.html.twig', [
            'simulation_duree' => $simulationDuree,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_simulation_duree_delete', methods: ['POST'])]
    public function delete(Request $request, SimulationDuree $simulationDuree, SimulationDureeRepository $simulationDureeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simulationDuree->getId(), $request->request->get('_token'))) {
            $simulationDureeRepository->remove($simulationDuree, true);
        }

        return $this->redirectToRoute('app_exercice_cp_simulation_duree_index', [], Response::HTTP_SEE_OTHER);
    }
}
