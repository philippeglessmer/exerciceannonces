<?php

namespace App\Controller\ExerciceCp;

use App\Entity\ExerciceCp\Collectivite;
use App\Form\ExerciceCp\CollectiviteType;
use App\Repository\ExerciceCp\CollectiviteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/collectivite')]
class CollectiviteController extends AbstractController
{
    #[Route('/', name: 'app_exercice_cp_collectivite_index', methods: ['GET'])]
    public function index(CollectiviteRepository $collectiviteRepository): Response
    {
        return $this->render('exercice_cp/collectivite/index.html.twig', [
            'collectivites' => $collectiviteRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_cp_collectivite_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CollectiviteRepository $collectiviteRepository): Response
    {
        $collectivite = new Collectivite();
        $form = $this->createForm(CollectiviteType::class, $collectivite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $collectiviteRepository->save($collectivite, true);

            return $this->redirectToRoute('app_exercice_cp_collectivite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/collectivite/new.html.twig', [
            'collectivite' => $collectivite,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_collectivite_show', methods: ['GET'])]
    public function show(Collectivite $collectivite): Response
    {
        return $this->render('exercice_cp/collectivite/show.html.twig', [
            'collectivite' => $collectivite,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_cp_collectivite_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Collectivite $collectivite, CollectiviteRepository $collectiviteRepository): Response
    {
        $form = $this->createForm(CollectiviteType::class, $collectivite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $collectiviteRepository->save($collectivite, true);

            return $this->redirectToRoute('app_exercice_cp_collectivite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_cp/collectivite/edit.html.twig', [
            'collectivite' => $collectivite,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_cp_collectivite_delete', methods: ['POST'])]
    public function delete(Request $request, Collectivite $collectivite, CollectiviteRepository $collectiviteRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$collectivite->getId(), $request->request->get('_token'))) {
            $collectiviteRepository->remove($collectivite, true);
        }

        return $this->redirectToRoute('app_exercice_cp_collectivite_index', [], Response::HTTP_SEE_OTHER);
    }
}
