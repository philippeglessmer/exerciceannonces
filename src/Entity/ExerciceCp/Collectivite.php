<?php

namespace App\Entity\ExerciceCp;

use App\Repository\ExerciceCp\CollectiviteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CollectiviteRepository::class)]
class Collectivite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $libelle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $siret = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cpt = null;

    #[ORM\Column(nullable: true)]
    private ?float $preAtt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $adresse = null;

    #[ORM\Column(nullable: true)]
    private ?int $codePostal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ville = null;

    #[ORM\OneToMany(mappedBy: 'collectivite', targetEntity: SimulationOffre::class)]
    private Collection $simulationOffres;

    public function __construct()
    {
        $this->simulationOffres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCpt(): ?string
    {
        return $this->cpt;
    }

    public function setCpt(?string $cpt): self
    {
        $this->cpt = $cpt;

        return $this;
    }

    public function getPreAtt(): ?float
    {
        return $this->preAtt;
    }

    public function setPreAtt(?float $preAtt): self
    {
        $this->preAtt = $preAtt;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(?int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection<int, SimulationOffre>
     */
    public function getSimulationOffres(): Collection
    {
        return $this->simulationOffres;
    }

    public function addSimulationOffre(SimulationOffre $simulationOffre): self
    {
        if (!$this->simulationOffres->contains($simulationOffre)) {
            $this->simulationOffres->add($simulationOffre);
            $simulationOffre->setCollectivite($this);
        }

        return $this;
    }

    public function removeSimulationOffre(SimulationOffre $simulationOffre): self
    {
        if ($this->simulationOffres->removeElement($simulationOffre)) {
            // set the owning side to null (unless already changed)
            if ($simulationOffre->getCollectivite() === $this) {
                $simulationOffre->setCollectivite(null);
            }
        }

        return $this;
    }
}
