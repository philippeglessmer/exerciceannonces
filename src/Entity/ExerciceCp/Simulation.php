<?php

namespace App\Entity\ExerciceCp;

use App\Repository\ExerciceCp\SimulationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SimulationRepository::class)]
class Simulation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $objet = null;

    #[ORM\ManyToOne(targetEntity: SimulationOffre::class, inversedBy: 'simulations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?SimulationOffre $offre;

    #[ORM\OneToMany(mappedBy: 'simulation', targetEntity: SimulationMontant::class, cascade: ["persist", "remove"])]
    private Collection $montant;

    #[ORM\OneToMany(mappedBy: 'simulation', targetEntity: SimulationDuree::class, cascade: ["persist", "remove"])]
    private Collection $Durees;

    #[ORM\OneToMany(mappedBy: 'simulation', targetEntity: SimulationFinancement::class, cascade: ["persist", "remove"])]
    private Collection $financements;

    #[ORM\ManyToOne(targetEntity: Simulation::class)]
    #[ORM\JoinColumn(name: "id", referencedColumnName: "id")]
    private ?Simulation $simulation;

    public function __construct()
    {
        $this->montant = new ArrayCollection();
        $this->Durees = new ArrayCollection();
        $this->financements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(?string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getOffre(): ?SimulationOffre
    {
        return $this->offre;
    }

    public function setOffre(?SimulationOffre $offre): self
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * @return Collection<int, SimulationMontant>
     */
    public function getMontants(): Collection
    {
        return $this->montant;
    }
    public function getMontant(): Collection
    {
        return $this->montant;
    }

    public function addMontant(SimulationMontant $montant): self
    {
        if (!$this->montant->contains($montant)) {
            $this->montant->add($montant);
            $montant->setSimulation($this);
        }

        return $this;
    }

    public function removeMontant(SimulationMontant $montant): self
    {
        if ($this->montant->removeElement($montant)) {
            // set the owning side to null (unless already changed)
            if ($montant->getSimulation() === $this) {
                $montant->setSimulation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SimulationDuree>
     */
    public function getDurees(): Collection
    {
        return $this->Durees;
    }

    public function addDuree(SimulationDuree $duree): self
    {
        if (!$this->Durees->contains($duree)) {
            $this->Durees->add($duree);
            $duree->setSimulation($this);
        }

        return $this;
    }

    public function removeDuree(SimulationDuree $duree): self
    {
        if ($this->Durees->removeElement($duree)) {
            // set the owning side to null (unless already changed)
            if ($duree->getSimulation() === $this) {
                $duree->setSimulation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SimulationFinancement>
     */
    public function getFinancements(): Collection
    {
        return $this->financements;
    }

    public function addFinancement(SimulationFinancement $financement): self
    {
        if (!$this->financements->contains($financement)) {
            $this->financements->add($financement);
            $financement->setSimulation($this);
        }

        return $this;
    }

    public function removeFinancement(SimulationFinancement $financement): self
    {
        if ($this->financements->removeElement($financement)) {
            // set the owning side to null (unless already changed)
            if ($financement->getSimulation() === $this) {
                $financement->setSimulation(null);
            }
        }

        return $this;
    }

    public function getSimulation(): ?Simulation
    {
        return $this->simulation;
    }
    public function setSimulation(?Simulation $simulation): self
    {
        $this->simulation = $simulation;

        return $this;
    }
}
