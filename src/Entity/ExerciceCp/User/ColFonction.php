<?php

namespace App\Entity\ExerciceCp\User;

use App\Repository\ExerciceCp\User\ColFonctionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ColFonctionRepository::class)]
class ColFonction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $libelle = null;

    #[ORM\ManyToOne(inversedBy: 'colFonctions')]
    private ?ColGenre $genre = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getGenre(): ?ColGenre
    {
        return $this->genre;
    }

    public function setGenre(?ColGenre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }
}
