<?php

namespace App\Entity\ExerciceCp\User;

use App\Repository\ExerciceCp\User\ColGenreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ColGenreRepository::class)]
class ColGenre
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $libelle = null;

    #[ORM\OneToMany(mappedBy: 'genre', targetEntity: ColFonction::class)]
    private Collection $colFonctions;

    public function __construct()
    {
        $this->colFonctions = new ArrayCollection();
    }

    public function __toString(){
        return $this->libelle;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, ColFonction>
     */
    public function getColFonctions(): Collection
    {
        return $this->colFonctions;
    }

    public function addColFonction(ColFonction $colFonction): self
    {
        if (!$this->colFonctions->contains($colFonction)) {
            $this->colFonctions->add($colFonction);
            $colFonction->setGenre($this);
        }

        return $this;
    }

    public function removeColFonction(ColFonction $colFonction): self
    {
        if ($this->colFonctions->removeElement($colFonction)) {
            // set the owning side to null (unless already changed)
            if ($colFonction->getGenre() === $this) {
                $colFonction->setGenre(null);
            }
        }

        return $this;
    }
}
