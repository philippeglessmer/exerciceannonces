<?php

namespace App\Entity;

use App\Repository\DepartementsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DepartementsRepository::class)]
class Departements
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $number = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'departements')]
    private ?Regions $regions;

    #[ORM\OneToMany(mappedBy: 'departements', targetEntity: Annonces::class)]
    private Collection $annonces;

    #[ORM\OneToMany(mappedBy: 'departement', targetEntity: VillesFrance::class)]
    private Collection $villesFrances;

    public function __construct()
    {
        $this->annonces = new ArrayCollection();
        $this->villesFrances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRegions(): ?Regions
    {
        return $this->regions;
    }

    public function setRegions(?Regions $regions): self
    {
        $this->regions = $regions;

        return $this;
    }

    /**
     * @return Collection<int, Annonces>
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonces $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces->add($annonce);
            $annonce->setDepartements($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonces $annonce): self
    {
        if ($this->annonces->removeElement($annonce)) {
            // set the owning side to null (unless already changed)
            if ($annonce->getDepartements() === $this) {
                $annonce->setDepartements(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, VillesFrance>
     */
    public function getVillesFrances(): Collection
    {
        return $this->villesFrances;
    }

    public function addVillesFrance(VillesFrance $villesFrance): self
    {
        if (!$this->villesFrances->contains($villesFrance)) {
            $this->villesFrances->add($villesFrance);
            $villesFrance->setDepartement($this);
        }

        return $this;
    }

    public function removeVillesFrance(VillesFrance $villesFrance): self
    {
        if ($this->villesFrances->removeElement($villesFrance)) {
            // set the owning side to null (unless already changed)
            if ($villesFrance->getDepartement() === $this) {
                $villesFrance->setDepartement(null);
            }
        }

        return $this;
    }
}
