<?php

namespace App\Entity\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\Lib\Contact;
use App\Repository\Repertoire\Acteur\ActeurContactRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActeurContactRepository::class)]
class ActeurContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $content = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $libelle = null;

    #[ORM\ManyToOne(inversedBy: 'acteurContacts')]
    private ?Contact $type = null;

    #[ORM\ManyToOne(inversedBy: 'relations')]
    private ?Acteur $acteur = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getType(): ?Contact
    {
        return $this->type;
    }

    public function setType(?Contact $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getActeur(): ?Acteur
    {
        return $this->acteur;
    }

    public function setActeur(?Acteur $acteur): self
    {
        $this->acteur = $acteur;

        return $this;
    }
}
