<?php

namespace App\Entity\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\Lib\Categorie;
use App\Entity\Repertoire\Acteur\Lib\Type;
use App\Repository\Repertoire\Acteur\ActeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActeurRepository::class)]
class Acteur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 200, nullable: true)]
    private ?string $raisonSocial = null;

    #[ORM\Column(nullable: true)]
    private ?int $etat = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $presentation = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'acteurs')]
    private ?Type $juridique = null;

    #[ORM\OneToMany(mappedBy: 'acteur', targetEntity: ActeurIdentification::class)]
    private Collection $identifications;

    #[ORM\OneToMany(mappedBy: 'acteur', targetEntity: ActeurAdresse::class)]
    private Collection $localisations;

    #[ORM\ManyToMany(targetEntity: Categorie::class, inversedBy: 'acteurs')]
    private Collection $categories;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?ActeurProjet $projet = null;

    #[ORM\OneToMany(mappedBy: 'acteur', targetEntity: ActeurContact::class)]
    private Collection $relations;

    #[ORM\OneToMany(mappedBy: 'acteur', targetEntity: ActeurMembre::class)]
    private Collection $membres;

    public function __construct()
    {
        $this->identifications = new ArrayCollection();
        $this->localisations = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->relations = new ArrayCollection();
        $this->membres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(?string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(?int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getJuridique(): ?Type
    {
        return $this->juridique;
    }

    public function setJuridique(?Type $juridique): self
    {
        $this->juridique = $juridique;

        return $this;
    }

    /**
     * @return Collection<int, ActeurIdentification>
     */
    public function getIdentifications(): Collection
    {
        return $this->identifications;
    }

    public function addIdentification(ActeurIdentification $identification): self
    {
        if (!$this->identifications->contains($identification)) {
            $this->identifications->add($identification);
            $identification->setActeur($this);
        }

        return $this;
    }

    public function removeIdentification(ActeurIdentification $identification): self
    {
        if ($this->identifications->removeElement($identification)) {
            // set the owning side to null (unless already changed)
            if ($identification->getActeur() === $this) {
                $identification->setActeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ActeurAdresse>
     */
    public function getLocalisations(): Collection
    {
        return $this->localisations;
    }

    public function addLocalisation(ActeurAdresse $localisation): self
    {
        if (!$this->localisations->contains($localisation)) {
            $this->localisations->add($localisation);
            $localisation->setActeur($this);
        }

        return $this;
    }

    public function removeLocalisation(ActeurAdresse $localisation): self
    {
        if ($this->localisations->removeElement($localisation)) {
            // set the owning side to null (unless already changed)
            if ($localisation->getActeur() === $this) {
                $localisation->setActeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Categorie>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categorie $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategory(Categorie $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getProjet(): ?ActeurProjet
    {
        return $this->projet;
    }

    public function setProjet(?ActeurProjet $projet): self
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * @return Collection<int, ActeurContact>
     */
    public function getRelations(): Collection
    {
        return $this->relations;
    }

    public function addRelation(ActeurContact $relation): self
    {
        if (!$this->relations->contains($relation)) {
            $this->relations->add($relation);
            $relation->setActeur($this);
        }

        return $this;
    }

    public function removeRelation(ActeurContact $relation): self
    {
        if ($this->relations->removeElement($relation)) {
            // set the owning side to null (unless already changed)
            if ($relation->getActeur() === $this) {
                $relation->setActeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ActeurMembre>
     */
    public function getMembres(): Collection
    {
        return $this->membres;
    }

    public function addMembre(ActeurMembre $membre): self
    {
        if (!$this->membres->contains($membre)) {
            $this->membres->add($membre);
            $membre->setActeur($this);
        }

        return $this;
    }

    public function removeMembre(ActeurMembre $membre): self
    {
        if ($this->membres->removeElement($membre)) {
            // set the owning side to null (unless already changed)
            if ($membre->getActeur() === $this) {
                $membre->setActeur(null);
            }
        }

        return $this;
    }
}
