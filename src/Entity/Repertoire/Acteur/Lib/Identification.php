<?php

namespace App\Entity\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\ActeurIdentification;
use App\Repository\Repertoire\Acteur\Lib\IdentificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IdentificationRepository::class)]
class Identification
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $libelle = null;

    #[ORM\Column(length: 150)]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'identifications', targetEntity: ActeurIdentification::class)]
    private Collection $acteurIdentifications;


    public function __toString(){
        return $this->libelle;
    }

    public function __construct()
    {
        $this->acteurIdentifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ActeurIdentification>
     */
    public function getActeurIdentifications(): Collection
    {
        return $this->acteurIdentifications;
    }

    public function addActeurIdentification(ActeurIdentification $acteurIdentification): self
    {
        if (!$this->acteurIdentifications->contains($acteurIdentification)) {
            $this->acteurIdentifications->add($acteurIdentification);
            $acteurIdentification->setIdentifications($this);
        }

        return $this;
    }

    public function removeActeurIdentification(ActeurIdentification $acteurIdentification): self
    {
        if ($this->acteurIdentifications->removeElement($acteurIdentification)) {
            // set the owning side to null (unless already changed)
            if ($acteurIdentification->getIdentifications() === $this) {
                $acteurIdentification->setIdentifications(null);
            }
        }

        return $this;
    }
}
