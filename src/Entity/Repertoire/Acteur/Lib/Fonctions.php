<?php

namespace App\Entity\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\ActeurMembre;
use App\Repository\Repertoire\Acteur\Lib\FonctionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FonctionsRepository::class)]
class Fonctions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $libelle = null;

    #[ORM\Column(length: 150)]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'fonction', targetEntity: ActeurMembre::class)]
    private Collection $acteurMembres;

    public function __toString(){
        return $this->libelle;
    }

    public function __construct()
    {
        $this->acteurMembres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ActeurMembre>
     */
    public function getActeurMembres(): Collection
    {
        return $this->acteurMembres;
    }

    public function addActeurMembre(ActeurMembre $acteurMembre): self
    {
        if (!$this->acteurMembres->contains($acteurMembre)) {
            $this->acteurMembres->add($acteurMembre);
            $acteurMembre->setFonction($this);
        }

        return $this;
    }

    public function removeActeurMembre(ActeurMembre $acteurMembre): self
    {
        if ($this->acteurMembres->removeElement($acteurMembre)) {
            // set the owning side to null (unless already changed)
            if ($acteurMembre->getFonction() === $this) {
                $acteurMembre->setFonction(null);
            }
        }

        return $this;
    }
}
