<?php

namespace App\Entity\Repertoire\Acteur\Lib;

use App\Entity\Repertoire\Acteur\ActeurContact;
use App\Repository\Repertoire\Acteur\Lib\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $libelle = null;

    #[ORM\Column(length: 150)]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: ActeurContact::class)]
    private Collection $acteurContacts;

    public function __toString(){
        return $this->libelle;
    }

    public function __construct()
    {
        $this->acteurContacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ActeurContact>
     */
    public function getActeurContacts(): Collection
    {
        return $this->acteurContacts;
    }

    public function addActeurContact(ActeurContact $acteurContact): self
    {
        if (!$this->acteurContacts->contains($acteurContact)) {
            $this->acteurContacts->add($acteurContact);
            $acteurContact->setType($this);
        }

        return $this;
    }

    public function removeActeurContact(ActeurContact $acteurContact): self
    {
        if ($this->acteurContacts->removeElement($acteurContact)) {
            // set the owning side to null (unless already changed)
            if ($acteurContact->getType() === $this) {
                $acteurContact->setType(null);
            }
        }

        return $this;
    }
}
