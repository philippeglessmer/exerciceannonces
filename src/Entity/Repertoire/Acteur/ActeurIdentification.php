<?php

namespace App\Entity\Repertoire\Acteur;

use App\Entity\Repertoire\Acteur\Lib\Identification;
use App\Repository\Repertoire\Acteur\ActeurIdentificationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActeurIdentificationRepository::class)]
class ActeurIdentification
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $content = null;

    #[ORM\ManyToOne(inversedBy: 'acteurIdentifications')]
    private ?Identification $identifications = null;

    #[ORM\ManyToOne(inversedBy: 'identifications')]
    private ?Acteur $acteur = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIdentifications(): ?Identification
    {
        return $this->identifications;
    }

    public function setIdentifications(?Identification $identifications): self
    {
        $this->identifications = $identifications;

        return $this;
    }

    public function getActeur(): ?Acteur
    {
        return $this->acteur;
    }

    public function setActeur(?Acteur $acteur): self
    {
        $this->acteur = $acteur;

        return $this;
    }
}
